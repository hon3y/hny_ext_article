<?php
namespace HIVE\HiveExtArticle\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ArticleTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtArticle\Domain\Model\Article
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtArticle\Domain\Model\Article();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMetaDataReturnsInitialValueForMetaData()
    {
    }

    /**
     * @test
     */
    public function setMetaDataForMetaDataSetsMetaData()
    {
    }

    /**
     * @test
     */
    public function getTaxonomyReturnsInitialValueForTag()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getTaxonomy()
        );
    }

    /**
     * @test
     */
    public function setTaxonomyForObjectStorageContainingTagSetsTaxonomy()
    {
        $taxonomy = new \HIVE\HiveExtTaxonomy\Domain\Model\Tag();
        $objectStorageHoldingExactlyOneTaxonomy = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneTaxonomy->attach($taxonomy);
        $this->subject->setTaxonomy($objectStorageHoldingExactlyOneTaxonomy);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneTaxonomy,
            'taxonomy',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addTaxonomyToObjectStorageHoldingTaxonomy()
    {
        $taxonomy = new \HIVE\HiveExtTaxonomy\Domain\Model\Tag();
        $taxonomyObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $taxonomyObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($taxonomy));
        $this->inject($this->subject, 'taxonomy', $taxonomyObjectStorageMock);

        $this->subject->addTaxonomy($taxonomy);
    }

    /**
     * @test
     */
    public function removeTaxonomyFromObjectStorageHoldingTaxonomy()
    {
        $taxonomy = new \HIVE\HiveExtTaxonomy\Domain\Model\Tag();
        $taxonomyObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $taxonomyObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($taxonomy));
        $this->inject($this->subject, 'taxonomy', $taxonomyObjectStorageMock);

        $this->subject->removeTaxonomy($taxonomy);
    }
}
