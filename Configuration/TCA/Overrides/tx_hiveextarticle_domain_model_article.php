<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';