## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextarticle {
        model {
            HIVE\HiveExtArticle\Domain\Model\Article {
                persistence {
                    storagePid = {$plugin.tx_hiveextarticle.model.HIVE\HiveExtArticle\Domain\Model\Article.persistence.storagePid}
                }
            }
        }
    }
}