<?php
namespace HIVE\HiveExtArticle\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Article
 */
class Article extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * metaData
     *
     * @var \HIVE\HiveExtMetadata\Domain\Model\MetaData
     */
    protected $metaData = null;

    /**
     * taxonomy
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtTaxonomy\Domain\Model\Tag>
     */
    protected $taxonomy = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->taxonomy = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the metaData
     *
     * @return \HIVE\HiveExtMetadata\Domain\Model\MetaData $metaData
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * Sets the metaData
     *
     * @param \HIVE\HiveExtMetadata\Domain\Model\MetaData $metaData
     * @return void
     */
    public function setMetaData(\HIVE\HiveExtMetadata\Domain\Model\MetaData $metaData)
    {
        $this->metaData = $metaData;
    }

    /**
     * Adds a Tag
     *
     * @param \HIVE\HiveExtTaxonomy\Domain\Model\Tag $taxonomy
     * @return void
     */
    public function addTaxonomy(\HIVE\HiveExtTaxonomy\Domain\Model\Tag $taxonomy)
    {
        $this->taxonomy->attach($taxonomy);
    }

    /**
     * Removes a Tag
     *
     * @param \HIVE\HiveExtTaxonomy\Domain\Model\Tag $taxonomyToRemove The Tag to be removed
     * @return void
     */
    public function removeTaxonomy(\HIVE\HiveExtTaxonomy\Domain\Model\Tag $taxonomyToRemove)
    {
        $this->taxonomy->detach($taxonomyToRemove);
    }

    /**
     * Returns the taxonomy
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtTaxonomy\Domain\Model\Tag> $taxonomy
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * Sets the taxonomy
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtTaxonomy\Domain\Model\Tag> $taxonomy
     * @return void
     */
    public function setTaxonomy(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $taxonomy)
    {
        $this->taxonomy = $taxonomy;
    }
}
