<?php
namespace HIVE\HiveExtArticle\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Articles
 */
class ArticleRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtArticle\\Domain\\Model\\Article';
        $sUserFuncPlugin = 'tx_hiveextarticle';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
