<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_article', 'Configuration/TypoScript', 'hive_ext_article');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextarticle_domain_model_article', 'EXT:hive_ext_article/Resources/Private/Language/locallang_csh_tx_hiveextarticle_domain_model_article.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextarticle_domain_model_article');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_article',
            'tx_hiveextarticle_domain_model_article'
        );

    }
);
